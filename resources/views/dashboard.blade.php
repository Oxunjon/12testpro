@extends('layout')
  
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>
  
                <div class="card-body">
                    @if (session('success'))
                        <div class="alert alert-success" role="alert">
                            {{ session('success') }}
                        </div>
                    @endif
  
                    You are Logged In
                    <div class="container">
                        <div class="row">
                        <form>
                            <input type="hidden" name="" value="<?php Auth::user()->id ?? ''  ?>">
  <div class="form-row">
    <div class="form-group col-md-6">
      <label for="inputEmail4">Email</label>
      <input type="email" class="form-control" id="inputEmail4" placeholder="Email">
    </div>
    <div class="form-group col-md-6">
      <label for="inputCity">Job</label>
      <input type="text" class="form-control" id="inputCity">
    </div>
  </div>
  <div class="form-group">
    <label for="inputAddress">Address</label>
    <input type="text" class="form-control" id="inputAddress" placeholder="1234 Main St">
  </div>
  <div class="form-group">
    <label for="inputAddress2">Address 2</label>
    <input type="text" class="form-control" id="inputAddress2" placeholder="Apartment, studio, or floor">
  </div>
  <div class="form-row">
    
   <div class="form-group col-md-12">
    <label for="inputCity">Job Description</label>
    <textarea name="description" class="form-control"></textarea>
    </div>
  </div>
  @if($amount)
 <?php  foreach($amount as $amoun) : ?>
  <?php dd($amoun);?>
  <button type="submit" class="btn btn-primary">Save job</button>
<?php endforeach; ?>
  @endif
</form>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection